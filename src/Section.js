import React, { Component } from 'react';

class Section extends Component {

  state = { active: false }

  componentDidMount() {
    window.addEventListener('scroll', this.onScroll)
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.onScroll)
  }

  onScroll = (e) => {
    const scrollTop = document.body.scrollTop + this.props.navHeight
    const offsetTop = this.element.offsetTop
    const height = this.element.offsetHeight
    const offset = offsetTop - scrollTop

    if (offset > 0) {
      // section is below top position
      this.setActive(false)
      return
    }
    if (offset + height >= 0) {
      //visible
      this.setActive(true)
    } else {
      // above visible area
      this.setActive(false)
    }
  }

  setActive(active) {
    if (active !== this.state.active) {
      this.setState({ active: active })

      this.props.setActive(this.props.anchor, active)
    }
  }

  render() {
    return (
      <section ref={(el) => {this.element = el}} id={this.props.anchor}>
        {this.props.children}
      </section>)
  }
}

export default Section
