import React, { Component } from 'react';
import './App.css';

import Section from './Section'
import Navigation from './Navigation'

const navItems = [
  { anchor: '1', title: 'Eins'},
  { anchor: '2', title: 'Zwei'},
  { anchor: '3', title: 'Drei'}
]

const TEXT = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet."

const NAV_HEIGHT = 30

class App extends Component {
  state = {
    activeSection: null
  }

  setActive = (section, active) => {
    if (active) {
      this.setState({ activeSection: section })
      return
    }

    if (section === this.state.activeSection) {
      this.setState({ activeSection: null })
      return
    }
  }

  render() {
    return (
      <div className="App">
        <Navigation navItems={navItems} activeSection={this.state.activeSection} />
        <div>
          {TEXT}
          <Section anchor='1' navHeight={NAV_HEIGHT} setActive={this.setActive}>{TEXT}</Section>
          <Section anchor='2' navHeight={NAV_HEIGHT} setActive={this.setActive}>{TEXT}</Section>
          {TEXT}
          <Section anchor='3' navHeight={NAV_HEIGHT} setActive={this.setActive}>{TEXT}</Section>
          {TEXT}
          {TEXT}
          {TEXT}
        </div>
      </div>
    );
  }
}

export default App;
