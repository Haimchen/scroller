import React, { Component } from 'react';

class Navigation extends Component {
  activeClass(anchor) {
    return anchor === this.props.activeSection ? 'active' : ''
  }

  render() {
    return (
      <nav>
        <div className="nav-inner">
          {this.props.navItems.map((nav) => (
            <a
              href={`#${nav.anchor}`}
              className={this.activeClass(nav.anchor)}
              key={nav.anchor}
            >
              <span>{nav.title}</span>
            </a>
            ))
          }
        </div>
      </nav>
    )
  }
}

export default Navigation
