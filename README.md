This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## Instructions

To test this project, run:
```
npm install
npm start
```

or

```
yarn install
yarn start
```

The app will then be available on localhost on Port 7000.
